.PHONY: help apply_patches unapply_patches
help:
	@echo 'Usage:                                                                    '
	@echo '   make apply_patches                  apply all the patches in patches/  '
	@echo '   make unapply_patches                discard all modification in vendor/'
	@echo '                                                                          '

apply_patches:
	for patch in `ls patches/*.patch`; do git apply $$patch; done

unapply_patches:
	git checkout -- vendor/
	git clean -f

