pelican-vendored
================

This repository contains theme and plugins used by
`GNU/Linux User Group Catania's website`_. Patches used to modify upstream
projects are inside ``patches/`` directory and are licensed with the same
license of upstream projects.

.. _GNU/Linux User Group Catania's website: https://gitlab.com/glugcatania/website

How to use
++++++++++

Clone this repository somewhere and use ``make apply_patches`` to apply all the
needed patches.
